import React, { Component } from 'react'
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Sign_in from './Sign_in/sign_in'
import List from './List/list'
import Detail from './Detail/detail'

export default class routes extends Component {
    render() {
        return (
            <div>
                 <BrowserRouter forceRefresh={false}>
                    <Switch>
                        <Route exact path='/' exact component={Sign_in} />
                        <Route exact path='/sign_in' exact component={Sign_in} />
                        <Route exact path='/list' exact component={List} />
                        <Route exact path='/detail' exact component={Detail} />
                


                    </Switch>
                </BrowserRouter>
            </div>
        )
    }
}
