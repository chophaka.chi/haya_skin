import React, { Component } from 'react'
// import Button from 'react-bootstrap/Button';
import { Container, Col, Row, Form, Button } from "react-bootstrap"
import logo from "../img/haya.PNG";
import "./sign_in.css"


export default class sign_in extends Component {
    render() {
        return (
            <div >
                {/* <Container fluid> */}
                <Row>
                    <Col><img style={{ height: "100vh", width: "100vh" }} src={logo} /></Col>
                    <Col style={{ padding: "12rem" }}>
                        {/* <div className="BoxLogin"> */}
                        <p style={{ fontSize: "20px" }}>เข้าสู่ระบบ</p>
                        <Form>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Username</Form.Label>
                                <Form.Control type="email" placeholder="Username" />
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" />
                            </Form.Group>
                            <Form.Label className="text-muted">
                                Forgot Password?
                            </Form.Label>
                            <div style={{ paddingTop: "1rem" }}>
                            <Button  variant="primary" type="submit" block  href="/list">
                                เข้าสู่ระบบ
                            </Button>
                            </div>
                        </Form>
                        {/* </div> */}
                    </Col>
                </Row>
                {/* </Container> */}
            </div>
        )
    }
}
