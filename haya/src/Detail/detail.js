import React, { Component } from 'react'
import { Nav, Col, Row, Form, Card, ListGroup, Button } from "react-bootstrap"
import "./detail.css"
import { BiLogOut } from 'react-icons/bi';
export default class detail extends Component {

    render() {
        return (
            <div >
                <Row >
                    {/* <Col  style={{position: "fixed", display: "flex"}}> */}
                    <Col md="2">
                        <Nav defaultActiveKey="/list" className="flex-column" style={{ position: "fixed", display: "flex", height: "100%", backgroundColor: "", padding: "2rem", fontSize: "14px" }} variant="pills">
                            <span style={{ fontSize: "28px", color: "#67abe8", textAlign: "center" }}>Haya Skin</span>
                            <b className="Containerfluid" style={{ fontSize: "16px", color: "back", marginTop: "2rem" }}>Admin Haya Skin</b>
                            <span className="Containerfluid" style={{ fontSize: "12px", color: "#adafb2", cursor: "pointer" }}> > เปลี่ยนรหัสผ่าน</span>
                            <span className="Containerfluid" style={{ fontSize: "14px", color: "#adafb2", marginTop: "2rem" }}> จัดการคำสั่งซื้อ</span>
                            <Nav.Item style={{ marginTop: "1rem" }}>
                                <Nav.Link href="/list" ><BiLogOut /> &nbsp;รายการคำสั่งซื้อ</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="link-1"><BiLogOut /> &nbsp;เพิ่มรายการสั่งซื้อ</Nav.Link>
                            </Nav.Item>
                            <span className="Containerfluid" style={{ fontSize: "14px", color: "#adafb2", marginTop: "2rem" }}> ยอดขาย</span>
                            <Nav.Item>
                                <Nav.Link eventKey="link-2" > <BiLogOut /> &nbsp;สถิติยอดขาย</Nav.Link>
                            </Nav.Item>
                            <div className="line"></div>
                            <Nav.Item style={{ marginTop: "1rem" }}>
                                <Nav.Link eventKey="link-3" href="/sign_in" > <BiLogOut /> &nbsp; ออกจากระบบ</Nav.Link>
                            </Nav.Item>

                        </Nav>
                    </Col>
                    <Col style={{ backgroundColor: "#f5f5f5", padding: "1rem" }} >
                        <Row >
                            <Col style={{ padding: "1rem", backgroundColor: "white" }}>
                                จัดการคำสั่งซื้อ
                        </Col>

                        </Row >
                        {/* <Col style={{ backgroundColor: "#f5f5f5" }}> */}
                        <Row style={{ paddingLeft: "1rem", paddingTop: "1rem" }} >
                            <Col md="6" >
                                <Card>
                                    <ListGroup.Item style={{ color: "white", backgroundColor: "#007bff", textAlign: "center", fontSize: "16px" }}>ข้อมูลการสั่งซื้อ</ListGroup.Item>
                                    <Card.Body>
                                        <Card.Text style={{ fontSize: "14px" }}>ข้อมูลผู้สั่งซื้อ</Card.Text>
                                        <div className="line-datail"></div>
                                        <Row style={{ paddingTop: "2rem", }}>
                                            <Col>
                                                <Form >
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>ชื่อผู้ส่ง</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="ชื่อผู้ส่ง" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>นามสกุลผู้ส่ง</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="นามสกุลผู้ส่ง" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                        </Row>
                                        <Row style={{ paddingTop: "0rem", }}>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>หมายเลขโทรศัพท์</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="หมายเลขโทรศัพท์" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "16px" }}>วันที่ส่ง</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="วว-ดด-ปปปป" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                        </Row>
                                        <Card.Text style={{ fontSize: "14px" }}>รายการสั่งซื้อ</Card.Text>
                                        <div className="line-datail"></div>
                                        <Row style={{ paddingTop: "2rem", }}>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>จำนวน/แพ็ก</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="0" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>ราคา/บาท</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="0.0" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                        </Row>
                                        <Card.Text style={{ fontSize: "14px" }}>สรุปยอด</Card.Text>
                                        <div className="line-datail"></div>
                                        <Row style={{ fontSize: "14px" }}>
                                            <Col>ราคา</Col>
                                            <Col style={{ textAlign: "right" }} >0.00 บาท</Col>

                                        </Row>
                                        <Row style={{ fontSize: "14px" }}>
                                            <Col >ค่าจัดส่ง</Col>
                                            <Col style={{ textAlign: "right" }}>0.00 บาท</Col>

                                        </Row>
                                        <div style={{ paddingTop: "1rem", }} className="line-datail"></div>
                                        <Row style={{ paddingTop: "1rem", fontSize: "14px" }}>
                                            <Col >ราคารวม</Col>
                                            <Col style={{ textAlign: "right" }}>0.00 บาท</Col>

                                        </Row>
                                        <p style={{ paddingTop: "3rem" ,color:"white"}}>
                                            5555
                                     </p>
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col md="6">
                                <Card>
                                    <ListGroup.Item style={{ color: "white", backgroundColor: "#007bff", textAlign: "center", fontSize: "16px" }}>การจัดส่ง</ListGroup.Item>
                                    <Card.Body>
                                        <Card.Text style={{ fontSize: "14px" }}>ที่อยู่ผู้จัดส่ง</Card.Text>
                                        <div className="line-datail"></div>
                                        <Row style={{ paddingTop: "2rem", }}>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>รหัสไปรษณี</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="รหัสไปรษณี" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>จังหวัด</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="จังหวัด" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                        </Row>
                                        <Row style={{ paddingTop: "0rem", }}>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>อำเภอ</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="อำเภอ" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>ตำบล</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="ตำบล" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>ที่อยู่เพิ่มเติม</Form.Label>
                                                            <Form.Control
                                                                as="textarea"
                                                                rows="3"
                                                                size="sm"
                                                                style={{ width: "30rem" }}
                                                                placeholder="ที่อยู่เพิ่มเติม"

                                                            />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>

                                        </Row>
                                        <Card.Text style={{ fontSize: "14px" }}>การจัดส่ง</Card.Text>
                                        <div className="line-datail"></div>
                                        <Row style={{ paddingTop: "1rem" }}>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>ตัวแทนที่จัดส่ง</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="ชื่อตัวแทนที่จัดส่ง" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>หมายเลขโทรศัพท์</Form.Label>
                                                            <Form.Control size="sm" type="email" placeholder="080-000-0000" />
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                        </Row>
                                        <Row style={{ paddingTop: "0rem" }}>
                                            <Col>
                                                <Form>
                                                    <Form.Row style={{ justifyContent: "center" }}>
                                                        <Form.Group controlId="formGridEmail">
                                                            <Form.Label style={{ fontSize: "14px" }}>สถานะ</Form.Label>
                                                            <Form.Control style={{ width: "11rem" }} size="sm" as="select" defaultValue="Choose...">
                                                                <option>รอการติดต่อกลับ</option>
                                                                <option>...</option>
                                                            </Form.Control>
                                                            {/* <Form.Control size="sm" type="email" placeholder="รอการติดต่อกลับ" /> */}
                                                        </Form.Group>
                                                    </Form.Row>
                                                </Form>
                                            </Col>
                                            <Col>

                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>

                                            </Col>
                                            <Col>

                                            </Col>
                                            <Col>
                                                <Button variant="primary" type="submit" block href="/list">
                                                    บันทึก
                                                </Button>
                                            </Col>

                                        </Row>
                                       
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>

                    </Col>
                    {/* </Col> */}
                </Row>

            </div>
        )
    }
}
