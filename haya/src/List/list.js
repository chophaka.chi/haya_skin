import React, { Component } from 'react'
import { Nav, Col, Row, Form, Button } from "react-bootstrap"
import "./list.css"
import { BiLogOut } from 'react-icons/bi';
import { MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import data from "../data"
export default class list extends Component {
    constructor(props) {
        
        super(props);
        this.state = {
            data: [
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                 
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                  
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                   
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                   
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
                {
                    
                    "name": "test",
                    "address": "test",
                    "phone": "test",
                    "status": "test",
                    "order_date": "test",
                    "sent_date" :"test",
                },
    
            ]
        };
    }
    click = () => {
        window.location.href = "/detail";

    };
    render() {
        const { data } = this.state;
        return (
            <div >
                <Row >
                    {/* <Col  style={{position: "fixed", display: "flex"}}> */}
                    <Col md="2">
                        <Nav defaultActiveKey="/list" className="flex-column" style={{ position: "fixed", display: "flex", height: "100%", backgroundColor: "", padding: "2rem", fontSize: "14px" }} variant="pills">
                            <span style={{ fontSize: "28px", color: "#67abe8", textAlign: "center" }}>Haya Skin</span>
                            <b className="Containerfluid" style={{ fontSize: "16px", color: "back", marginTop: "2rem" }}>Admin Haya Skin</b>
                            <span className="Containerfluid" style={{ fontSize: "12px", color: "#adafb2", cursor: "pointer" }}> > เปลี่ยนรหัสผ่าน</span>
                            <span className="Containerfluid" style={{ fontSize: "14px", color: "#adafb2", marginTop: "2rem" }}> จัดการคำสั่งซื้อ</span>
                            <Nav.Item style={{ marginTop: "1rem" }}>
                                <Nav.Link href="/list" ><BiLogOut /> &nbsp;รายการคำสั่งซื้อ</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="link-1"><BiLogOut /> &nbsp;เพิ่มรายการสั่งซื้อ</Nav.Link>
                            </Nav.Item>
                            <span className="Containerfluid" style={{ fontSize: "14px", color: "#adafb2", marginTop: "2rem" }}> ยอดขาย</span>
                            <Nav.Item>
                                <Nav.Link eventKey="link-2" > <BiLogOut /> &nbsp;สถิติยอดขาย</Nav.Link>
                            </Nav.Item>
                            <div className="line"></div>
                            <Nav.Item style={{ marginTop: "1rem" }}>
                                <Nav.Link eventKey="link-3"  href="/sign_in" > <BiLogOut /> &nbsp; ออกจากระบบ</Nav.Link>
                            </Nav.Item>

                        </Nav>
                    </Col>
                    <Col style={{ backgroundColor: "#f5f5f5"  }}>
                        <Row >
                            <Col style={{ padding: "1rem", backgroundColor: "white" }}>
                                จัดการคำสั่งซื้อ
                            </Col>

                        </Row>
                        {/* <Col style={{ backgroundColor: "#f5f5f5" }}> */}
                        <Row style={{ paddingLeft: "1rem", paddingTop: "1rem" }} >
                            <Col md="4">
                                <Form>

                                    <Form.Label>ค้นหา</Form.Label>
                                    <Form.Control type="email" placeholder="ค้นหา" />

                                </Form>
                            </Col>
                            <Col md="3">
                                <Form.Label>สถานะ</Form.Label>
                                <Form.Control as="select" defaultValue="Choose...">
                                    <option>สถานะ</option>
                                    <option>...</option>
                                </Form.Control>
                            </Col>
                        </Row>
                        <Row style={{paddingLeft: "2rem", paddingTop: "1rem",textAlign:"center" }}>
                            <MDBTable style={{width:"75rem"}}> 
                                <MDBTableHead color="primary-color"  textWhite>
                                    <tr >
                                        <th >ลำดับ</th>
                                        <th >ชื่อผู้สั่ง</th>
                                        <th>ที่อยู่</th>
                                        <th>เบอร์โทร</th>
                                        <th>สถานะ</th>
                                        <th>วันที่สั่ง</th>
                                        <th>วันที่ส่ง</th>
                                    </tr>
                                </MDBTableHead>
                                <MDBTableBody>
                                {data.length > 0
                            ? data.map((index, i) => (
                                    <tr onClick={this.click} style={{ cursor: "pointer" }}>
                                        <td>{i + 1}</td>
                                        <td>{index.name}</td>
                                        <td>{index.address}</td>
                                        <td>{index.phone}</td>                                    
                                        <td>{index.status}</td>
                                        <td>{index.order_date}</td>
                                        <td>{index.sent_date}</td>
                                    </tr>
                                    ))
                                    : null}
                                   
                                </MDBTableBody>
                            </MDBTable>
                        </Row>
                    </Col>
                    {/* </Col> */}
                </Row>
               
            </div>
        )
    }
}
